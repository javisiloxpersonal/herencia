package com.inheritance.herencia.usescase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inheritance.herencia.R
import com.inheritance.herencia.globals.GlobalAdapter
import com.inheritance.herencia.models.Vehiculo

class MainAdapter(vehiculos : MutableList<Vehiculo>) : GlobalAdapter<Vehiculo>(vehiculos){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MotosViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle, parent, false)
        return MotosViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MotosViewHolder).bind(getItem(position))
    }

    inner class MotosViewHolder(var itemV : View) : RecyclerView.ViewHolder(itemV){

        fun bind(vehiculo: Vehiculo?){
            var textNumPlazas = itemV.findViewById<TextView>(R.id.textNumPlazas)
            var textCombustible = itemV.findViewById<TextView>(R.id.textCombustible)

            textNumPlazas.text = vehiculo?.plazas.toString()
            textCombustible.text = vehiculo?.combustible.toString()

        }

    }

}