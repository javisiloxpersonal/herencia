package com.inheritance.herencia.models

class Coche(plazas : Int, combustible : Int) : Vehiculo(plazas, combustible){

    override fun goInto() {
        //Abrir la puerta entrar y sentarse y abrocharme el cinturon
    }

    private fun abrocharCinturon(){
        //estira el brazo, coge la evilla, anclala
    }

}