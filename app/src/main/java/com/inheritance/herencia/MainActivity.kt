package com.inheritance.herencia

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.inheritance.herencia.globals.COMBS_DIESEL
import com.inheritance.herencia.globals.COMBS_GAOSLINA
import com.inheritance.herencia.globals.COMBS_GAS
import com.inheritance.herencia.models.*
import com.inheritance.herencia.usescase.HornoAdapter
import com.inheritance.herencia.usescase.MainAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var vehiculos : MutableList<Vehiculo> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //var moto = Coche(2, COMBS_GAOSLINA)
        //var bus = Coche(25, COMBS_GAS)

        var coche = Coche(5, COMBS_DIESEL)
        var moto = Moto(2, COMBS_GAOSLINA)
        var bus = Bus(20, COMBS_GAS)


        vehiculos.add(coche)
        vehiculos.add(moto)
        vehiculos.add(bus)

        vehiculos.forEach {
            it.startMotor()
            it.goInto()
        }

        initCocheAdapter()
    }

    fun pinta(position : Int){
        (recyclerVehicle.adapter as MainAdapter).getItem(position)
    }

    private fun initMotoAdapter(){
        var motosList = mutableListOf<Vehiculo>()

        for(i in 0 .. 10){
            motosList.add(Moto(i, 2))
        }

        recyclerVehicle.adapter = MainAdapter(motosList)
    }

    private fun initCocheAdapter(){
        var cochesList = mutableListOf<Vehiculo>()

        for(i in 0 .. 10){
            cochesList.add(Coche(i*-1, 2))
        }

        recyclerVehicle.adapter = MainAdapter(cochesList)
    }

    private fun initHornoAdapter(){
        var hornos = mutableListOf<Horno>()

        for(i in 0 .. 10){
            hornos.add(Horno(i, "blanco"))
        }

        recyclerVehicle.adapter = HornoAdapter(hornos)
    }
}
